# whisperbot

Telegram inline bot which lets you send a message in a group, which can only be accessed by a given user.

# Get started

1. Copy `config.json.example` to `config.json` and change the token to your own bot token.
2. Run `npm i` to get all dependencies
3. Run `npm run` to start it
