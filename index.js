// First load config for early failure
const config = require('./config.json');

// the time this program was created
// it's subtracted from the current time to create IDs
const TIME_OFFSET = 1606396608166;


// Then load all dependencies
const { Telegraf } = require('telegraf');
const Keyv = require('keyv');
const { KeyvFile } = require('keyv-file');
const path = require('path');


// open simple key-value store
const secrets = new Keyv({
	store: new KeyvFile({
		filename: path.join(path.resolve(__dirname), './secrets.json'),
	}),
	writeDelay: 1000, // ms
});


// make bot
const bot = new Telegraf(config.token);


// error handling
bot.catch((err) => console.error(`${+new Date} bot error`, err));
secrets.on('error', (err) => console.error(`${+new Date} keyv error`, err));


// get own username
let username = "?";
bot.telegram.getMe().then((me) => {
	username = me.username;
	console.log(`${+new Date} I am ${me.first_name} (@${username})`);
});


// handle /start
bot.start((ctx) => {
	ctx.reply(`I work inline only. Try typing @${username} in a group chat.`);
});


// handle button press
bot.on('callback_query', async (ctx) => {
	// extract necessary data
	const q       = ctx.update.callback_query;
	const id      = q.data;
	const clicker = q.from;
	const message = await secrets.get(id);

	const [_from_id, target] = id.split(':');
	const from_id = parseInt(_from_id);

	// message not found in secrets
	if (!message) {
		console.warn(`${+new Date} callback_query '${id}' not found in database; clicker=${JSON.stringify(clicker)}`);
		ctx.answerCbQuery("Internal error: Please ping @Cxarli");
		return;
	}

	// target not found in identifier
	if (!target) {
		console.warn(`${+new Date} callback_query '${id}' does not contain target; clicker=${JSON.stringify(clicker)}`);
		ctx.answerCbQuery("Internal error: Please ping @Cxarli");
		return;
	}

	// check clicker and target/source
	if (clicker.username.toLowerCase() !== target.toLowerCase() && clicker.id !== from_id) {
		console.log(`${+new Date} Message refused to open`);
		ctx.answerCbQuery(`This message is not for you`);
		return;
	}

	// answer
	console.log(`${+new Date} Message first opened by ${clicker.id === from_id ? 'sender' : 'target'}`);
	ctx.answerCbQuery(message, /*showAlert*/ true, { cache_time: 3600 });
});


bot.on('chosen_inline_result', (ctx) => {
	// extract necessary data
	const update = ctx.update.chosen_inline_result;
	const query = update.query;

	// no text? just wait
	if (!query) return;

	// extract target username and message
	const reg = query.match(/^@(\w{4,}) (.*)/i);
	if (!reg) return; // no match
	if (!reg[1]) return; // no username
	if (!reg[2]) return; // no message

	// extract more information
	const from = update.from.id;
	const target = reg[1];
	const message = reg[2];

	// store secret
	const id = update.result_id;
	secrets.set(id, message);
	console.log(`${+new Date} New secret created`);
});

// handle inline query
bot.on('inline_query', (ctx) => {
	// extract necessary data
	const update = ctx.update.inline_query;
	const query = update.query;

	// no text? just wait
	if (!query) return;

	// extract target username and message
	const reg = query.match(/^@(\w{4,}) (.*)/i);
	if (!reg) return; // no match
	if (!reg[1]) return; // no username
	if (!reg[2]) return; // no message

	// extract more information
	const from = update.from.id;
	const target = reg[1];
	const message = reg[2];

	// generate ID
	const id = `${from}:${target}:${new Date - TIME_OFFSET}`;

	// answer
	ctx.answerInlineQuery([{
		type: 'article',
		id: id,
		title: query,
		input_message_content: {
			message_text: `<i>Secret message to <b>@${target}</b> and <b>@${target}</b> only.</i>`,
			parse_mode: "html",
		},

		reply_markup: {
			inline_keyboard: [[{
				text: "Reveal",
				callback_data: id,
			}]],
		},
	}], {
		cache_time: 0,
		is_personal: true,
	});
});


// Launch bot
bot.launch().then(() => {
	console.log(`${+new Date} Started at ${new Date}`);
});
